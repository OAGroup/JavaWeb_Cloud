
<h1 align="center"> 为梦想而创作：JavaWeb_Cloud微服务分布式开发平台</h1>

### 项目介绍
JavaWeb_Cloud微服务平台是一款基于SpringCloud框架研发的分布式微服务框架，主要使用技术栈包括： SpringCloud、Vue、ElementUI、MybatisPlus，是一款精心打造的权限(RBAC)及内容管理系统，致力于做更简洁的后台管理框架，包含系统管理、代码生成、权限管理、站点、广告、布局、字段、配置等一系列常用的模块，整套系统一键生成所有模块（包括前端UI），一键实现CRUD，简化了传统手动抒写重复性代码的工作。
同时，框架提供长大量常规组件，如上传单图、上传多图、上传文件、下拉选择、复选框按钮、单选按钮，城市选择、富文本编辑器、权限颗粒度控制等高频使用的组件，代码简介，使用方便，节省了大量重复性的劳动，降低了开发成本，提高了整体开发效率，整体开发效率提交80%以上，专注于为中小企业提供最佳的行业基础后台框架解决方案，执行效率、扩展性、稳定性值得信赖，操作体验流畅，使用非常优化，欢迎大家使用及进行二次开发。

## 开发者信息
* 系统名称：JavaWeb权限(RBAC)及内容管理框架  
* 作者：鲲鹏
* 作者QQ：1175401194  
* 官网网址：[http://www.javaweb.vip/](http://www.javaweb.vip/)  
* 文档网址：[http://docs.cloud.javaweb.vip](http://docs.cloud.javaweb.vip)

## 后台演示
- 演示地址：[http://manage.cloud.javaweb.vip](http://manage.cloud.javaweb.vip)
- 登录用户名：admin
- 登录密码：123456

## 技术支持

[技术支持QQ：1175401194](http://wpa.qq.com/msgrd?v=3&amp;uin=1175401194&amp;site=qq&amp;menu=yes)


## 效果图展示

#### 系统登录
 ![系统登录](http://images.cloud.javaweb.vip/views/1.png)

#### 后台首页
 ![后台首页](http://images.cloud.javaweb.vip/views/2.png)
 
#### 用户管理
 ![用户管理](http://images.cloud.javaweb.vip/views/3.png)
 
#### 角色管理
 ![角色管理](http://images.cloud.javaweb.vip/views/4.png)
 
#### 职级管理
 ![职级管理](http://images.cloud.javaweb.vip/views/5.png)
 
#### 岗位管理
![岗位管理](http://images.cloud.javaweb.vip/views/6.png)

#### 部门管理
![部门管理](http://images.cloud.javaweb.vip/views/7.png)

#### 菜单管理
![菜单管理](http://images.cloud.javaweb.vip/views/8.png)

#### 代码生成器
![代码生成器](http://images.cloud.javaweb.vip/views/9.png)

#### 项目结构
![项目结构](http://images.cloud.javaweb.vip/views/10.png)


## 安全&缺陷
如果你发现了一个安全漏洞或缺陷，请发送邮件到 1175401194@qq.com,所有的安全漏洞都将及时得到解决。


## 鸣谢
感谢[MybatisPlus](https://mp.baomidou.com/)、[JQuery](http://jquery.com)等优秀开源项目。

## 版权信息

JavaWeb商业版使用需授权，未授权禁止恶意传播和用于商业用途，否则将追究相关人的法律责任。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2019-2020 javaweb.vip (http://www.javaweb.vip)

All rights reserved。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
